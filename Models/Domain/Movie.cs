﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
         // Pk
        public int Id { get; set; }

        // Fields
        [Required]
        [MaxLength(50)]
        public string MovieTitle { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
        public DateTime ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(50)]
        public string Picture { get; set; }
        [MaxLength(50)]
        public string Trailer { get; set; }

        // Relationships
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }
    }
}
