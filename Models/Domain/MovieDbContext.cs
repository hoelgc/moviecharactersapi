using MovieCharactersAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models
{
    public class MovieDbContext : DbContext
    {
        // Tables
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string databaseCamilla = "ND-5CG92747K7\\SQLEXPRESS;";
            string databaseNicole = "ND-5CG9030M98\\SQLEXPRESS";

            optionsBuilder.UseSqlServer("Data Source=ND-5CG92747K7\\SQLEXPRESS;Initial Catalog=MovieCharactersDB;Integrated Security=True;");
        }*/
        public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Franchise>().HasData(new Franchise() { Id = 1, Name = "Lord of The Rings", Description = "Set in the fictional world of Middle-earth, the films follow the hobbit Frodo Baggins as he and the Fellowship embark on a quest to destroy the One Ring, to ensure the destruction of its maker, the Dark Lord Sauron." });

            modelBuilder.Entity<Movie>().HasData(
                new Movie { Id = 1, MovieTitle = "The Fellowship of the Ring", FranchiseId = 1 },
                new Movie { Id = 2, MovieTitle = "The Two Towers", FranchiseId = 1 },
                new Movie { Id = 3, MovieTitle = "The Return of the King", FranchiseId = 1 });

            modelBuilder.Entity<Character>().HasData(new Character() { Id = 1, Name = "Frodo Baggins", Gender = "Male", Picture = "" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 2, Name = "Gandalf", Gender = "Male", Picture = "" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 3, Name = "Aragorn", Gender = "Male", Picture = "" });

            // Seed m2m Character-Movie. Need to define m2m and access linking table
            modelBuilder.Entity<Character>()
                .HasMany(m => m.Movies)
                .WithMany(c => c.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 1, MovieId = 3 },
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 3 },
                            new { CharacterId = 3, MovieId = 1 },
                            new { CharacterId = 3, MovieId = 2 },
                            new { CharacterId = 3, MovieId = 3 }
                        );
                    });
        }
    }
}
