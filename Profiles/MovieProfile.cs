﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        { 
            CreateMap<MovieCreateDTO, Movie>();

            CreateMap<Movie, MovieReadDTO>()
                    .ForMember(cdto => cdto.Characters, opt => opt
                    .MapFrom(c => c.Characters.Select(c => c.Id).ToArray()));

            CreateMap<MovieEditDTO, Movie>();

        }
    }
}
