﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        { 
            CreateMap<FranchiseCreateDTO, Franchise>();

            CreateMap<Franchise, FranchiseReadDTO>()
                    .ForMember(cdto => cdto.Movies, opt => opt
                    .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));

            CreateMap<FranchiseEditDTO, Franchise>();
        }

    }
}
