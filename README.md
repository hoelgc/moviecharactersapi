# Assignment 3 - Create a Web API and document it
## Appendix A: EF Core Code First
Constructed an application in ASP.NET Core where the database was made in SQL Server through EF Core.
Created the entities Character, Movie and Franchise.
* The Movie table has a many-to-many relationship with the Character table
* The Franchise table has a one-to-many relationship with the Movie table

Used Microsoft SQL Server Management Studio to connect to the database created and to view the changes made to the database.

### Folders
#### Migrations
* The migration file for initializing the database
* The migration file for seeding the database

## Appendix B: Web API using ASP.NET Core
Unfortunately we weren't able to complete Appendix B. We tried our best, but didn't have enough time or energi to complete it.
The methods GetAllMoviesInFranchise(), GetAllCharactersInFranchise() and EditMovie() in the FranchiseController and EditCharacter in MovieController are not finished.


### Folders
#### Controllers
The Controllers folder holds the code for the user interaction

#### Models
The Models folder is where all the Business logic is (Character, Movie and Franchise classes).
* As well as the DbContext class

#### Profiles
These classes are used for the automapping.
